/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DataStructures;

/**
 *
 * @author Admin
 */
public class DoublyLinkedList {

    static Node head;

    static class Node {

        Node next;
        Node previous;
        int data;

        Node(int data) {

            this.data = data;
            this.next = null;
            this.previous = null;

        }
    }

    void push(int data) {

        Node temp = new Node(data);
        if (head != null) {
            head.previous = temp;
            temp.previous = null;
            temp.next = head;
            head = temp;

        } else {

            head = temp;
            head.previous = null;
            head.next = null;
        }

    }

    void insertAfter(Node prev, int data) {

        Node new_node = new Node(data);
        Node temp = null;

        if (prev == null) {
            System.out.println("Previous node cant be NULL");
        }

        new_node.next = prev.next;
        prev.next = new_node;
        new_node.previous = prev;

    }

    void append(int data) {
        Node new_node = new Node(data);
        Node tnode = head;

        if (tnode == null) {

            head = new_node;
            head.previous = null;
            head.next = null;
        }

        while (tnode != null && tnode.next != null) {
            tnode = tnode.next;

        }

        tnode.next = new_node;
        new_node.previous = tnode;
        new_node.next = null;

    }

    void addBefore(Node after, int data) {

        Node new_node = new Node(data);

        if (head == null) {
            System.out.println("The list is empty");
        }
        
        if(after==head){
        
            new_node.previous = null;
            new_node.next = head;
            head.previous = new_node;
            head = new_node;
        }

        Node tnode = head;

        while (tnode != after && tnode.next != null) {

            tnode = tnode.next;

        }
        if (tnode == after) {

            Node Before = tnode.previous;
            new_node.previous = Before;
            new_node.next = tnode;

        } else {
            System.out.println("Previous node NOT FOUND");
        }

    }

    void printAll() {

        Node tnode = head;

        while (tnode != null) {

            System.out.print(tnode.data + ", ");
            tnode = tnode.next;
        }

    }

    public static void main(String[] args) {

        DoublyLinkedList link = new DoublyLinkedList();

        link.push(1);
        link.push(2);
        link.push(33);
        link.push(45);

        link.printAll();

        link.insertAfter(head, 56);
        System.out.println("\nAfter inserting after head");
        link.printAll();
        System.out.println("\nAfter inserting at the end");
        link.append(78);
        link.printAll();
        System.out.println("\nAfter inserting 77 before head ");
        link.addBefore(head, 77);
        link.printAll();
        

    }

}
