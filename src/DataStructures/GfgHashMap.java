/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DataStructures;

/**
 *
 * @author Admin
 */


// Java program to illustrate 
// Java.util.HashMap 
import java.util.HashMap; 
import java.util.Map; 

public class GfgHashMap 
{ 
	public static void main(String[] args) 
	{ 
	
		HashMap<String, Integer> map = new HashMap<>(); 
		
		print(map); 
		map.put("vishal", 10); 
		map.put("sachin", 30); 
		map.put("vaibhav", 20); 
		
		System.out.println("Size of map is:- " + map.size()); 
	
		print(map); 
		if (map.containsKey("vishal")) 
		{ 
			Integer a = map.get("vishal");
                        Integer b= map.hashCode();
			System.out.println("value for key \"vishal\" is: " + a + " The maps hashcode is: "+ b); 
		} 
		
		map.clear(); 
		print(map); 
	} 
	
	public static void print(Map<String, Integer> map) 
	{ 
		if (map.isEmpty()) 
		{ 
			System.out.println("map is empty"); 
		} 
		
		else
		{ 
			System.out.println(map); 
		} 
	} 
} 

