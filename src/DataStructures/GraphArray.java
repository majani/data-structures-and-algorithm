/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DataStructures;

import java.util.LinkedList;
import java.util.Iterator;

/**
 *
 * @author Admin
 */
public class GraphArray {
    static class Graph {

        public int V;
        LinkedList<Integer>[] adjListArray;
        
        Graph(int V) {
            this.V = V;

            adjListArray = new LinkedList[V];
            for (int i = 0; i < V; i++) {
                adjListArray[i] = new LinkedList<>();
            }
        }
    }

    // Adds an edge to an undirected graph 
    static void addEdge(Graph graph, int src, int dest) {
        // Add an edge from src to dest.  
        graph.adjListArray[src].add(dest);

        // Since graph is undirected, add an edge from dest 
        // to src also 
        graph.adjListArray[dest].add(src);
    }
    
    
   static int sumOfDependencies(Graph graph, int V){
    
        int sum = 0;
        
        for(int i=0;i<graph.adjListArray.length;i++)
        
            sum += graph.adjListArray[i].size();
        return sum;
    }
   
   
   static void BFS(int src, Graph graph){
       int V = graph.V;
   
       boolean[] visited = new boolean[V];
       
       LinkedList<Integer> queue = new LinkedList<>();
       visited[src] = true;
       queue.add(src);
       
       while(!queue.isEmpty()){
       
           src = queue.poll();
           System.out.println(src);
           
           Iterator<Integer> i = graph.adjListArray[src].listIterator();
           while(i.hasNext()){
           
               int n = i.next();
               if(!visited[n]){
               visited[n]= true;
               queue.add(n);
               
               }
           }
       }
       
       
       
   
   }

    // A utility function to print the adjacency list  
    // representation of graph 
    static void printGraph(Graph graph) {
        for (int v = 0; v < graph.V; v++) {
            System.out.println("Adjacency list of vertex " + v);
            System.out.print("head");
            for (Integer pCrawl : graph.adjListArray[v]) {
                System.out.print(" -> " + pCrawl);
            }
            System.out.println("\n");
        }
    }

    // Driver program to test above functions 
    public static void main(String args[]) {
        // create the graph given in above figure 
        int V = 5;
        Graph graph = new Graph(V);
        addEdge(graph, 0, 1);
        addEdge(graph, 0, 4);
        addEdge(graph, 1, 2);
        addEdge(graph, 1, 3);
        addEdge(graph, 1, 4);
        addEdge(graph, 2, 3);
        addEdge(graph, 3, 4);

        // print the adjacency list representation of  
        // the above graph 
        printGraph(graph);
        System.out.println("Sum of dependencies is " +sumOfDependencies(graph, V)); 
        
        System.out.println("Following is Breadth First Traversal "
                + "(starting from vertex 2)");

        BFS(2,graph);
    }

}
