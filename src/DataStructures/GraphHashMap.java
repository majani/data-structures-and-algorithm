/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DataStructures;

import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;

/**
 *
 * @author Admin
 */
public class GraphHashMap {

    HashMap<Integer, LinkedList<Integer>> map = new HashMap<>();

//add an edge from source to destination
    void addEdge(int src, int dest) {
        
        
        if (!map.containsKey(src)) {
            LinkedList<Integer> l = new LinkedList<>();
            l.add(dest);
            map.put(src, l);
        } else {
            map.get(src).add(dest);
        }
    }
    
   

//display the adjacency list
    void displayGraph() {
        for (Map.Entry m : map.entrySet()) {
            System.out.println(m.getKey() + "-->" + m.getValue());
        }
    }
    
    

    public static void main(String[] args) {

        GraphHashMap g = new GraphHashMap();
        g.addEdge(0, 1);
        g.addEdge(0, 4);
        g.addEdge(0, 5);
        g.addEdge(1, 4);
        g.addEdge(1, 3);
        g.addEdge(3, 2);
        g.addEdge(2, 1);
        g.addEdge(3, 4);
        g.addEdge(4, 1);
        g.displayGraph();
        
    }
}
