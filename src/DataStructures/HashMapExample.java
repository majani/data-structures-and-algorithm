/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DataStructures;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;
import java.util.Map;

/**
 *
 * @author Admin
 */
public class HashMapExample {

    public static void main(String[] args) {

        HashMap<Integer, String> hmap = new HashMap<>();
        /*Adding elements to HashMap*/
        hmap.put(12, "Chaitanya");
        hmap.put(2, "Rahul");
        hmap.put(7, "Singh");
        hmap.put(49, "Ajeet");
        hmap.put(3, "Anuj");

        /* Display content using Iterator*/
       

       for(Map.Entry mentry: hmap.entrySet()){
            System.out.print("key is: " + mentry.getKey() + " & Value is: ");
            System.out.println(mentry.getValue());

        }
        /* Get values based on key*/
        String var = hmap.get(2);
        System.out.println("Value at index 2 is: " + var);

        /* Remove values based on key*/
        hmap.remove(3);
        System.out.println("Map key and values after removal:");
        

       for(Map.Entry mentry2: hmap.entrySet()){
           
            System.out.print("Key is: " + mentry2.getKey() + " & Value is: ");
            System.out.println(mentry2.getValue());
       }
        

    }

}
