/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DataStructures;

/**
 *
 * @author Admin
 */
// A complete working Java program to demonstrate deletion in singly 
// linked list 
class LinkedListDelete {

    Node head; // head of list 

    /* Linked list Node*/
    static class Node {

        int data;
        Node next;

        Node(int d) {
            data = d;
            next = null;
        }
    }
   


    /* Given a key, deletes the first occurrence of key in linked list */
    void deleteNode(int key) {
        // Store head node 
        Node temp = head, prev = null;

        // If head node itself holds the key to be deleted 
        if (temp != null && temp.data == key) {
            head = temp.next; // Changed head 
            return;
        }

        // Search for the key to be deleted, keep track of the 
        // previous node as we need to change temp.next 
        while (temp != null && temp.data != key) {
            prev = temp;
            temp = temp.next;
        }

        // If key was not present in linked list 
        if (temp == null) {
            return;
        }

        // Unlink the node from linked list 
        prev.next = temp.next;
    }

    /* Inserts a new Node at front of the list. */
    public void push(int new_data) {
        Node new_node = new Node(new_data);
        new_node.next = head;
        head = new_node;
    }

    //Print the middle node of a linked list
    void printMiddle() {
        Node slow_ptr = head;
        Node fast_ptr = head;
        if (head != null) {
            while (fast_ptr != null && fast_ptr.next != null) {
                fast_ptr = fast_ptr.next.next;
                slow_ptr = slow_ptr.next;
            }
            System.out.println("The middle element is [" + slow_ptr.data + "] \n");
        }
    }

    void insertNewNode(Node prev, int new_data) {

        Node new_node = new Node(new_data);
        if (prev == null) {
            System.out.println("Previous node cant be empty");
        }

        prev.next = new_node;
        new_node.next = prev.next;

    }

    //Checks whether the value x is present in linked list 
    public boolean search(Node head, int x) {
        Node current = head;    //Initialize current 
        while (current != null) {
            if (current.data == x) {
                return true;    //data found 
            }
            current = current.next;
        }
        return false;    //data not found 
    }

    /* This function prints contents of linked list starting from 
		the given node */
    public void printList() {
        Node tnode = head;
        while (tnode != null) {
            System.out.print(tnode.data + " ");
            tnode = tnode.next;
        }
    }
    
    

    /* Drier program to test above functions. Ideally this function 
	should be in a separate user class. It is kept here to keep 
	code compact */
    public static void main(String[] args) {

        LinkedListDelete llist = new LinkedListDelete();
        
        

        llist.push(7);
        llist.push(1);
        llist.push(3);
        llist.push(2);
       
        

        System.out.println("\nCreated Linked list is:");
        llist.printList();

        llist.deleteNode(3); // Delete node at position 4 

        System.out.println("\nLinked List after Deletion at position 4:");
        llist.printList();

        llist.printMiddle();

        Boolean found = llist.search(llist.head, 3);

        System.out.println("Its " + found + " that the node is present in the linked list");

       
        
        

    }
}
