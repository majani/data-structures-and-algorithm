/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DataStructures;

import java.util.HashSet;
import java.util.Iterator;

/**
 *
 * @author Admin
 */
public class LinkedListMe {

    Node head;
    HashSet<Integer> set = new HashSet<>();

    static class Node {

        int data;
        Node next;

        Node(int data) {
            this.data = data;
            this.next = null;

        }

    }

    void insertNodeBegining(int data) {
        Node new_node = new Node(data);
        if (head != null) {

            new_node.next = head;
            head = new_node;
        } else {
            head = new_node;
        }
    }

    void insertAfterNode(int in, int data) {
        Node temp = new Node(data);
        temp.next = null;

        Node prev = null;
        Node current = head;

        if (current == null) {
            System.out.println("Head can not be NULL");
        }

        if (current.data == in) {

            temp.next = current.next;
            current.next = temp;

        }

        while (current.data != in && current.next != null) {

            prev = current;
            current = current.next;

        }

        if (current.data == in) {

            temp.next = current.next;
            current.next = temp;

        } else {
            System.out.println("Point of insertion NOT FOUND");
        }

    }

    void insertAtEnd(int data) {
        Node temp = new Node(data);
        Node current = head;


        if (current.next == null) {

            head.next = temp;
        }

        while (current.next != null) {

            current = current.next;
        }

        if (current.next == null) {

            current.next = temp;
        }

        System.out.println("Last node not inserted");

    }

    public int findMiddle() {
        Node fast = head;
        Node slow = head;

        if (head != null) {

            while (fast != null && fast.next != null) {

                fast = fast.next.next;
                slow = slow.next;
            }

        }
        return slow.data;

    }

    void deleteHead() {

        if (head != null) {
            head = head.next;

        }

    }
    
    void addToSet(){
    
        Node current = head;
        
        if(current == null){
        System.out.println("The List is empty");
        }else{
        
        while(current!=null){
        
            set.add(current.data);
            current = current.next;
        }}
        
        
        for(int s: set){
        System.out.println(s);
        }
    
    }
    
    
    
    void deleteDuplicates(){
        Node current = head;
        Node prev = null;
       
        
        if (current == null){
        System.out.println("The List is empty");
        }
        
        while(current!=null){
        
            if(set.contains(current.data)){
            
                prev.next = current.next;
            }
            else{
                set.add(current.data);
            
                prev = current;
                current = current.next;
                
            }
        }
        
        
    
        
    }

    void deleteNode(int key) {

        Node prev = null;
        Node temp = head;

        if (temp == null) {

            System.out.print("Head can not be NULL");
        }

        if (temp.data == key) {

            head = head.next;
            return;

        }

        while (temp.data != key) {

            prev = temp;
            temp = temp.next;

        }

        prev.next = temp.next;
    }

    void printNodes() {

        Node tnode = head;
        while (tnode != null) {

            System.out.print(tnode.data + ", ");
            tnode = tnode.next;
        }
    }

    void push(int data) {
        Node new_node = new Node(data);
        new_node.next = head;
        head = new_node;

    }

    public static void main(String[] args) {

        LinkedListMe link = new LinkedListMe();

        link.push(1);
        link.push(2);
        link.push(33);
        link.push(45);
        link.push(23);
        link.push(101);
        link.push(5);
        link.push(17);

        link.printNodes();
        link.deleteHead();
        System.out.println("\nAfter deleting head");
        link.printNodes();
        link.insertNodeBegining(899);
        System.out.println("\nAfter inserting at the beginning");
        link.printNodes();
        System.out.println("\nThe middle of the node is " + link.findMiddle());
        link.insertAfterNode(5, 22);
        link.insertAfterNode(5, 22);
        System.out.println("\nAfter inserting a new node after 5");
        link.printNodes();
        
         
        link.insertAtEnd(34);
        System.out.println("\nAfter inserting a new node(34) at the end");
        link.printNodes();
        link.deleteNode(899);
        System.out.println("\nAfter deleting node(899) at the head");
        link.printNodes();
        link.deleteNode(33);
        System.out.println("\nAfter deleting node(33) in the middle");
        link.printNodes();
        
        System.out.println("\nAfter adding to a set");
        
        link.addToSet();
       

    }

}
