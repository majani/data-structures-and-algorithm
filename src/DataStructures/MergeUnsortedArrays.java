/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DataStructures;

import java.util.*;

/**
 *
 * @author Admin
 */
public class MergeUnsortedArrays {

    public static void mergeArrays(int[] a, int[] b, int[] res, int m, int n) {

        int i = 0;
        int j = 0;
        int k = 0;

        while (i < n) {

            res[k] = a[i];
            i++;
            k++;
        }

        while (j < m) {

            res[k] = b[j];
            j++;
            k++;
        }

        Arrays.sort(res);

    }

    public static void main(String[] args) {

        int[] a = {1, 4, 6, 8, 3, 77, 43, 22};
        int[] b = {67, 89, 9, 34, 76, 83, 29, 43};

        int m = a.length;
        int n = b.length;
        int k = m + n;
        int res[] = new int[m + n];
        mergeArrays(a, b, res, n, m);

        System.out.print("Array A\n ");
        for (int i = 0; i < m; i++) {
            System.out.print(a[i] + " ");
        }

        System.out.print("\n Array B\n ");
        for (int i = 0; i < n; i++) {
            System.out.print(b[i] + " ");
        }

        System.out.print("\n Sorted Array \n ");
        for (int i = 0; i < k; i++) {
            System.out.print(res[i] + " ");
        }

    }

}
