/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DataStructures;

import java.util.LinkedList;

/**
 *
 * @author Admin
 */
public class SumOfDependencies {

    static int V;
    LinkedList<Integer> adj[];

    SumOfDependencies(int v) {

        this.V = v;
        adj = new LinkedList[V];

        for (int i = 0; i < V; i++) {

            adj[i] = new LinkedList();
        }

    }

    // To add an edge 
    void addEdge(int u, int v) {
        adj[u].add((v));
    }

    // find the sum of all dependencies 
    int findSum(int V) {
        int sum = 0;

        // just find the size at each vector's index 
        for (int i = 0; i < V; i++) {
            sum += adj[i].size();
        }

        return sum;
    }

    // Driver method 
    public static void main(String[] args) {

        SumOfDependencies s = new SumOfDependencies(4);

        s.addEdge(0, 2);
        s.addEdge(0, 3);
        s.addEdge(1, 3);
        s.addEdge(2, 3);

        System.out.println("Sum of dependencies is " + s.findSum(V));
    }

}
