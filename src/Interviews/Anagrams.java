/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interviews;

/**
 *
 * @author Admin
 */
import java.io.*;
import java.util.*;

public class Anagrams {

    // Complete the makeAnagram function below.
    static int makeAnagram(String a, String b) {
        int[] letterCounts = new int[26];
        for(char c : a.toCharArray()){
            letterCounts[c-'a']++;
        }

        for(char c:b.toCharArray()){
            letterCounts[c-'a']--;
        }

        int result=0;
        for(int i:letterCounts){
            result += Math.abs(i);
        }

        return result;


    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

        String a = scanner.nextLine();

        String b = scanner.nextLine();

        int res = makeAnagram(a, b);

        bufferedWriter.write(String.valueOf(res));
        bufferedWriter.newLine();

        bufferedWriter.close();

        scanner.close();
    }
}

