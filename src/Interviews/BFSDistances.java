/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interviews;

/**
 *
 * @author Admin
 */
import java.util.*;

public class BFSDistances {
    public static class Graph {

        int size;
        LinkedList<Integer> adj[];
        
        
        public Graph(int size) {
            this.size = size;
            adj =  new LinkedList[size];

            for(int i =0;i<size;i++){
                adj[i]= new LinkedList<>();
            }
            
        }

        public void addEdge(int first, int second) {
            adj[first].add(second);
            adj[second].add(first);
            
        }
        
        public int[] shortestReach(int startId) { 
        LinkedList<Integer> queue = new LinkedList<>();
        int[] distances = new int[size];
        Arrays.fill(distances, -1);

        
        queue.add(startId);
        distances[startId] = 0;

        while(!queue.isEmpty()){
            startId = queue.poll();
            Iterator<Integer> i = adj[startId].listIterator();

            while(i.hasNext()){
                int n = i.next();
                if(distances[n]==-1){
                    distances[n] = distances[startId]+6;
                    queue.add(n);

                }
            }
        }

        return distances;
            
        }
    }
    
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
      
        int queries = scanner.nextInt();
        
        for (int t = 0; t < queries; t++) {
            
            // Create a graph of size n where each edge weight is 6:
            Graph graph = new Graph(scanner.nextInt());
            int m = scanner.nextInt();
            
            // read and set edges
            for (int i = 0; i < m; i++) {
                int u = scanner.nextInt() - 1;
                int v = scanner.nextInt() - 1;
                
                // add each edge to the graph
                graph.addEdge(u, v);
            }
            
            // Find shortest reach from node s
            int startId = scanner.nextInt() - 1;
            int[] distances = graph.shortestReach(startId);
 
            for (int i = 0; i < distances.length; i++) {
                if (i != startId) {
                    System.out.print(distances[i]);
                    System.out.print(" ");
                }
            }
            System.out.println();            
        }
        
        scanner.close();
    }
}


