/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interviews;

/**
 *
 * @author Admin
 */
public class Fibonacci {

    static int fib(int n) {

        if (n <= 1) {
            return n;
        }

        return fib(n - 1) + fib(n - 2);

    }

    static int dynamicFib(int n) {

        int[] f = new int[n + 1];
        int i;
        f[0] = 0;
        f[1] = 1;

        for (i = 2; i <= n; i++) {

            f[i] = f[i - 1] + f[i - 2];
        }

        return f[n];
    }

    static int ultraLeanFib(int n) {

        int a = 0;
        int b = 1;
        int c;

        if (n == 0) 
            return a;
        

        for (int i = 2; i <=n;i++){
        c = a + b;
            a = b;
            b = c;
        }
        
        return b;

    }

    public static void main(String[] args) {

        int n = 12;
        System.out.println("Normal Fib " + fib(n));
        System.out.println("Dynamic Fib " + dynamicFib(n));
        System.out.println("UltraLean Fib " + ultraLeanFib(n));
    }

}
