/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interviews;

public class GCD
{
    
    static int gcd(int a, int b){
        if(a==0)return b;
        return gcd(b%a,a);
    }
    // METHOD SIGNATURE BEGINS, THIS METHOD IS REQUIRED
    public int generalizedGCD(int num, int[] arr)
    {
        int result = arr[0];
        for(int i=0;i<num;i++){
            result = gcd(arr[i],result);
        }
        
        return result;
        
    }
    // METHOD SIGNATURE ENDS
}
