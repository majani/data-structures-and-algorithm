/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interviews;

/**
 *
 * @author Admin
 */
import java.util.*;

public class Matrix {

    public static int hourGlass(int[][] anArray, int i, int j) {

        int result;
        result = anArray[i][j] + anArray[i][j + 1] + anArray[i][j + 2] + anArray[i + 1][j + 1]
                + anArray[i + 2][j] + anArray[i + 2][j + 1] + anArray[i + 2][j + 2];
        return result;
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int arr[][] = new int[6][6];
        for (int i = 0; i < 6; i++) {
            for (int j = 0; j < 6; j++) {
                arr[i][j] = in.nextInt();
            }
        }

        int ans = 0, tempAns = -63;
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                ans = hourGlass(arr, i, j);
                if (ans > tempAns) {
                    tempAns = ans;
                }
            }
        }

        if (ans >= tempAns) {
            System.out.println(ans);
        } else {
            System.out.println(tempAns);
        }
    }
}
