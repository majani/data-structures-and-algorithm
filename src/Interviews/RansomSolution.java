/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interviews;

/**
 *
 * @author Admin
 */
import java.util.*;

public class RansomSolution {

    private static final Scanner scanner = new Scanner(System.in);

    // Complete the checkMagazine function below.
    static String checkMagazine(String[] magazine, String[] note) {

        HashMap<String, Integer> magMap = new HashMap<>();
        
        for (String word : magazine) {
            if (magMap.containsKey(word)) {
                int count = magMap.get(word);
                count++;
                magMap.put(word, count);
            } else {
                magMap.put(word, 1);
            }
        }

        HashMap<String, Integer> noteMap = new HashMap<>();

        for (String word : note) {
            if (noteMap.containsKey(word)) {
                int count = noteMap.get(word);
                count++;
                noteMap.put(word, count);
            } else {
                noteMap.put(word, 1);
            }
        }

      

       for(Map.Entry i : noteMap.entrySet()) {
            String word = i.getKey().toString();
            int count = (Integer)i.getValue();

            if (!magMap.containsKey(word)) {
                return "No";
            } else if (count > magMap.get(word)) {
                return "No";
            }

        }

        return "Yes";

    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        int m = in.nextInt();
        int n = in.nextInt();

        String[] magazine = new String[m];

        for (int i = 0; i < m; i++) {
            magazine[i] = in.next();
        }

        String[] note = new String[n];

        for (int i = 0; i < n; i++) {
            note[i] = in.next();
        }

        System.out.print(checkMagazine(magazine, note));

        in.close();
    }
    
}
