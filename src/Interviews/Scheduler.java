/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interviews;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author Admin
 */
public class Scheduler {

    private static ScheduledExecutorService executor;

    public static void main(String[] args) {

        executor = Executors.newSingleThreadScheduledExecutor();

        ScheduledFuture<?> future = solution(()->System.out.println("Hello World"), 10000);

        executor.shutdown();
        executor = null;

    }

    public static ScheduledFuture<?> solution(Runnable command, int delays_ms) {

        return executor.schedule(command, delays_ms, TimeUnit.MILLISECONDS);

    }

}
