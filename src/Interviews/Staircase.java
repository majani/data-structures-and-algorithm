/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interviews;

/**
 *
 * @author Admin
 *
 * There exists a staircase with N steps, and you can climb up either 1 or 2
 * steps at a time. Given N, write a function that returns the number of unique
 * ways you can climb the staircase. The order of the steps matters.
 *
 * For example, if N is 4, then there are 5 unique ways:
 *
 * 1, 1, 1, 1 
 * 2, 1, 1 
 * 1, 2, 1 
 * 1, 1, 2 
 * 2, 2
 */
public class Staircase {

    static int fib(int n) {

        int a = 0;
        int b = 1;
        int c;
        
        if(n<=1){
        return n;
        }

        for (int i = 2; i <= n; i++) {

            c = a + b;
            a = b;
            b = c;
        }

        return b;

    }

    static int ways(int n) {

        return fib(n + 1);
    }
    
    
    static int countWaysUtil(int n, int m){
    
       int res[] = new int[n];
       res[0] = 1;
       res[1]=1;
       
       for(int i =2;i<n;i++){
       
           res[i]= 0;
           for(int j = 1;j<=m&& j<=i;j++)
           res[i] += res[i-j];
       }
       
       return res[n-1];
        
    }
    
    static int countWays(int n,int m){
    
        return countWaysUtil(n+1,m);
    }

    public static void main(String[] args) {

        int n = 5;
        int m = 4;
        System.out.println("Ways of going up this stairs " + ways(n));
        System.out.println("Ways of going up this stairs in given amount of ways (5) " + countWays(n,m));
    }

}
