/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interviews;

import java.util.HashMap;

/**
 *
 * @author Admin
 */
public class Substring {

    public static void main(String[] args) {

        String s = "karappa";
        int start = 0;
        int end = 0;
        int windowSize = 1;
        int windowStart = 0;
        int m = 3;
        

        HashMap<Integer, Integer> hash = new HashMap<>();
        int ch = s.charAt(0)-'a';
        hash.put(ch, 1);

        for (int i = 1; i < s.length(); i++) {

            ch = s.charAt(i)-'a';

            if (!hash.containsKey(ch)) {

                hash.put(ch, 1);
            } else {

                hash.put(ch,hash.get(ch)+1);
            }

            end++;

            if (hash.size() > m) {
                int temp = hash.get(s.charAt(start)-'a');
                hash.put(s.charAt(start)-'a', --temp);
                start++;

            }

            if (end - start + 1 > windowSize) {

                windowSize = end - start + 1;
                windowStart = start;
            }
        }

        String sub = s.substring(windowStart, windowStart + windowSize);
        System.out.println(sub);

    }
}
