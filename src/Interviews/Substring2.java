/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interviews;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author Admin
 */
public class Substring2 {

    static int solutions (String s, int k) {

        
      
      String maxLengthString = " ";
      
      if(s.length()!=0 && k!= 0){
      
          StringBuilder window = new StringBuilder();
          Set<Character> set = new HashSet<>();
          
          for (int i = 0; i < s.length(); i++) 
        {
            char ch = s.charAt(i);
            if (set.size() == k && !set.contains(ch)) 
            {
                // Fetch the last index of the first character at window. Discard the string
                // before the last index.
                window = new StringBuilder(
                        window.substring(window.lastIndexOf(Character.toString(window.charAt(0))) + 1));
                set.clear();
                for (int j = 0; j < window.length(); j++) 
                {
                    set.add(window.charAt(j));
                }
            }
            set.add(ch);
            window.append(ch);
            if (window.length() > maxLengthString.length()) 
            {
                maxLengthString = window.toString();
            }
        }
    }

    System.out.println("String with max length is " + maxLengthString);
    return maxLengthString.length();

}



}
