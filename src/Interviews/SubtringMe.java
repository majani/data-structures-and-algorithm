/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interviews;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author Admin
 */
public class SubtringMe {

    public static void main(String[] args) {

        String s = "karappaaa";
        int start = 0;
        int end = 0;
        String sub = "";
        int k = 2;
        int WindowSize = 0;
        StringBuilder window = new StringBuilder();
        Set<Character> set = new HashSet<>();

        HashMap<Character, Integer> map = new HashMap<>();

        for (int i = 0; i < s.length(); i++) {

            char ch = s.charAt(i);
            if (map.containsKey(ch)) {

                map.put(ch, map.get(ch) + 1);
            } else {

                map.put(ch, 1);
            }

            end++;

            if ((map.size() == k) && (!map.containsKey(ch))) {
                start++;
            }
            sub = s.substring(start,end-start);

        }
        
        
        System.out.println(sub);

    }
}
