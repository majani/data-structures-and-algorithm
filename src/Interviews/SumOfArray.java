/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interviews;

import java.util.HashSet;

/**
 *
 * @author Admin
 */
public class SumOfArray {

    public static boolean findSum(int[] a, int x) {
        int n = a.length;

        HashSet<Integer> hash = new HashSet<>();
        boolean flag = false;

        for (int i = 0; i < n; i++) {

            hash.add(a[i]);
            if (hash.contains(x - a[i])) {
                flag = true;
            }
        }

        return flag;

    }

    public static void main(String[] args) {

        int[] a = {3, 5, 7, 10};
        int x = 17;

        System.out.println(findSum(a, x));

    }

}
