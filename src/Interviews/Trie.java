/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interviews;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Admin
 */
public class Trie {

    private final TrieNode root;

    private class TrieNode {

        Map<Character, TrieNode> children;
        boolean endOfWord;

        public TrieNode() {

            children = new HashMap<>();
            endOfWord = false;
        }
    }

    public Trie() {

        root = new TrieNode();
    }

    /**
     * Iterative implementation of insert into trie
     *
     * @param word
     */
    public void insert(String word) {

        TrieNode current = root;
        for (int i = 0; i < word.length(); i++) {

            char ch = word.charAt(i);

            TrieNode node = current.children.get(ch);

            if (node == null) {

                node = new TrieNode();
                current.children.put(ch, node);
            }

            current = node;

        }
        //mark the current nodes endOfWord as true
        current.endOfWord = true;
    }

    /**
     * Recursive implementation of search into trie.
     *
     * @param word
     */
    public boolean searchRecursive(String word) {
        return searchRecursive(root, word, 0);

    }

    private boolean searchRecursive(TrieNode current, String word, int index) {

        if (index == word.length()) {

            //return true of current's endOfWord is true else return false.
            return current.endOfWord;
        }

        char ch = word.charAt(index);
        TrieNode node = current.children.get(ch);
        //if node does not exist for given char then return false
        if (node == null) {

            return false;
        }

        return searchRecursive(node, word, index + 1);

    }

    /**
     * Delete word from trie.
     */
    public void delete(String word) {

        delete(root, word, 0);
    }

    /**
     * Returns true if parent should delete the mapping
     */
    private boolean delete(TrieNode current, String word, int index) {

        if (index == word.length()) {
            //when end of word is reached only delete if currrent.endOfWord is true.
            if (!current.endOfWord) {

                return false;
            }
            current.endOfWord = false;
            //if current has no other mapping then return true
            return current.children.size() == 0;
        }

        char ch = word.charAt(index);
        TrieNode node = current.children.get(ch);
        if (node == null) {

            return false;
        }

        boolean shouldDeleteCurrentNode = delete(node, word, index + 1);
        //if true is returned then delete the mapping of character and trienode reference from map.

        if (shouldDeleteCurrentNode) {

            current.children.remove(ch);
            //return true if no mappings are left in the map.
            return current.children.size() == 0;
        }
        return false;
    }

}
