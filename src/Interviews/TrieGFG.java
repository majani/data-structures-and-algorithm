/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interviews;

/**
 *
 * @author Admin
 */
public class TrieGFG {

    static final int ALPHABET_SIZE = 26;
    private static TrieNode root;

    static class TrieNode {

        TrieNode[] children = new TrieNode[ALPHABET_SIZE];
        boolean endOfWord = false;

        public TrieNode() {

            for (int i = 0; i < ALPHABET_SIZE; i++) {

                children[i] = null;
            }

            endOfWord = false;
        }
    }

    public TrieGFG() {

        root = new TrieNode();
    }

    static void insert(String word) {

        TrieNode current = root;

        for (int i = 0; i < word.length(); i++) {

            int index = word.charAt(i) - 'a';
            if (current.children[index] == null) {
                current.children[index] = new TrieNode();
            }

            current = current.children[index];

        }

        current.endOfWord = true;
    }

    static boolean search(String word) {

        TrieNode current = root;
        for (int i = 0; i < word.length(); i++) {

            int index = word.charAt(i) - 'a';
            if (current.children[index] == null) {
                return false;
            }
            current = current.children[index];

        }
        return ((current != null) && (current.endOfWord));

    }

    public static void main(String[] args) {

        String[] words = {"the", "a", "there", "answer", "any", "by", "bye", "their"};
        String[] output = {"Not present in trie", "Present in trie"};

        root = new TrieNode();

        for (String word : words) {
            insert(word);
        }

        // Search for different keys 
        if (search("the") == true) {
            System.out.println("the --- " + output[1]);
        } else {
            System.out.println("the --- " + output[0]);
        }

        if (search("these") == true) {
            System.out.println("these --- " + output[1]);
        } else {
            System.out.println("these --- " + output[0]);
        }

        if (search("their") == true) {
            System.out.println("their --- " + output[1]);
        } else {
            System.out.println("their --- " + output[0]);
        }

        if (search("thaw") == true) {
            System.out.println("thaw --- " + output[1]);
        } else {
            System.out.println("thaw --- " + output[0]);
        }
    }

}
