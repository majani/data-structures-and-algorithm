/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interviews;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Admin
 */
public class TrieMe {

    private TrieNode root;

    class TrieNode {

        Map<Character, TrieNode> children;
        boolean endOfWord;

        public TrieNode() {

            children = new HashMap<>();
            endOfWord = false;
        }
    }

    public TrieMe() {

        root = new TrieNode();
    }

    public void insert(String word) {

        TrieNode current = root;

        for (int i = 0; i < word.length(); i++) {

            char ch = word.charAt(i);

            TrieNode node = current.children.get(ch);

            if (node == null) {
                
                node = new TrieNode();

                current.children.put(ch, node);
            }

            current = node;
        }
        current.endOfWord = true;
    }

    public List<String> search(String word) {

        List<String> results = new ArrayList<>();

        TrieNode node = findNode(word, root, 0);
        if (node == null) {

            return results;
        }

        findWords(node, new StringBuilder(word), results);
        return results;
    }

    private TrieNode findNode(String word, TrieNode current, int index) {

        if (index == word.length()) {

            return current;
        }

        Character ch = word.charAt(index);
        TrieNode node = current.children.get(ch);
        if (node == null) {

            return null;
        }

        return findNode(word, node, ++index);

    }

    private void findWords(TrieNode current, StringBuilder sb, List<String> results) {

        for (Map.Entry ch : current.children.entrySet()) {
            StringBuilder word = new StringBuilder(sb).append(ch);
            if (current.endOfWord) {
                results.add(word.toString());
            }
            findWords(current, word, results);
        };

    }

}

class TestDrive {

    public static void main(String args[]) {
        TrieMe trie = new TrieMe();//Arrays.asList(new String[]{"dog", "dee", "deer", "deal"})
        trie.insert("dog");
        trie.insert("dee");
        trie.insert("deer");
        trie.insert("deal");

        trie.search("de").forEach(System.out::println);
        System.out.println();

        trie.search("do").forEach(System.out::println);
        System.out.println();
    }
}
