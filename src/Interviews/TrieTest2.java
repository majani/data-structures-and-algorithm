/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interviews;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Admin
 */

class TrieNode {

    public Character content;
    public TrieNode[] children = new TrieNode[26];
    public boolean isWord;

    TrieNode(Character ch) {
        this.content = ch;
    }
}

class TrieT {

    TrieNode root;

    TrieT() {
        root = new TrieNode('*');
    }

    public void insert(String word) {
        TrieNode currentNode = root;
        for (int i = 0; i < word.length(); i++) {
            Character ch = word.charAt(i);
            if (currentNode.children[ch - 'a'] == null) {
                currentNode.children[ch - 'a'] = new TrieNode(ch);
            }
            currentNode = currentNode.children[ch - 'a'];

        }
        currentNode.isWord = true;
    }

    public List<String> search(String searchTxt, TrieNode currentNode, int index) {
        List<String> results = new ArrayList<>();
        Character ch = searchTxt.charAt(index);
        if (currentNode.children[ch - 'a'] == null) {
            return results;
        }
        currentNode = currentNode.children[ch - 'a'];
        if (index == searchTxt.length() - 1) {
            findWords(currentNode, new StringBuilder(searchTxt), results);
            return results;
        }
        return search(searchTxt, currentNode, ++index);
    }

    public void findWords(TrieNode currentNode, StringBuilder sb, List<String> results) {

        for (TrieNode child : currentNode.children) {
            if (child != null) {
                if (child.isWord == true) {
                    results.add(sb.append(child.content).toString());
                }
                findWords(child, new StringBuilder(sb).append(child.content), results);
            }
        }
    }

}

class DailyCodingProble11 {

    public static void main(String args[]) {

        String[] words = {"dog", "deer", "deal"};
        TrieT trie = new TrieT();
        for (String word : words) {
            trie.insert(word);
        }
        trie.search("de", trie.root, 0).stream().forEach(word -> System.out.println(word));
        trie.search("do", trie.root, 0).stream().forEach(word -> System.out.println(word));
    }
}
