/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Patterns;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.lang.reflect.Constructor;

/**
 *
 * @author Admin
 */
public class Singleton implements Serializable {
    
    private static Singleton soleInstance;

    private Singleton()

    {
        System.out.println("Creating..");
    }
    
    public static Singleton getInstance(){
        
        if(soleInstance == null){ 
        soleInstance = new Singleton();
        }
    
        return soleInstance;
    }

}

class TestClass {

    public static void main(String[] args) throws Exception {
    
        Singleton s1 = Singleton.getInstance();
        Singleton s2 = Singleton.getInstance();
        
        print("S1", s1);
        print("S2", s2);
        
        //Reflections
        Class clazz = Class.forName("Patterns.Singleton");
        Constructor<Singleton> ctor =  clazz.getDeclaredConstructor();
        ctor.setAccessible(true);
        Singleton s3 = ctor.newInstance();
        
        print("S3", s3);
        
        //Serialization
        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("C:\\Users\\Admin\\Documents\\NetBeansProjects\\Sorting\\src\\Patterns\\file.txt"));
        oos.writeObject(s2);
        
        ObjectInputStream ois = new ObjectInputStream(new FileInputStream("C:\\Users\\Admin\\Documents\\NetBeansProjects\\Sorting\\src\\Patterns\\file.txt"));
        Singleton s4 = (Singleton) ois.readObject();
        
        print("S4",s4);
        
        
        
        
    }
    
    static void print(String name, Singleton object){
    
        System.out.println(String.format("Object: %s, Hashcode: %d", name, object.hashCode()));
    }

}