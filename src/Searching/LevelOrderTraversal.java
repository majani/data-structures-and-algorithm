/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Searching;

/**
 *
 * @author Admin
 */
public class LevelOrderTraversal {

    Node root;

    static class Node {

        int data;
        Node left = null;
        Node right = null;

        public Node(int data) {

            this.data = data;
            this.left = null;
            this.right = null;
        }

    }

    public LevelOrderTraversal() {

        root = null;
    }

    /* Compute the "height" of a tree -- the number of 
    nodes along the longest path from the root node 
    down to the farthest leaf node.*/
    int height(Node root) {

        if (root == null) {
            return 0;
        } else {
            int lheight = height(root.left);
            int rheight = height(root.right);

            if (lheight > rheight) {

                return lheight + 1;
            } else {

                return rheight + 1;
            }

        }
    }

    /* Print nodes at the given level */
    void printGivenLevel(Node root, int level) {
        if (root == null) {

            return;
        }

        if (level == 1) {

            System.out.println(root.data + " ");
        } else if (level > 1) {

            printGivenLevel(root.left, level - 1);
            printGivenLevel(root.right, level - 1);
        }

    }
    
    /* function to print level order traversal of tree*/

    void printLevelOrder() {

        int h = height(root);
        int i;
        for (i = 1; i < h; i++) {

            printGivenLevel(root, i);
        }

    }
    
    public static void main(String args[]) 
    { 
       LevelOrderTraversal tree = new LevelOrderTraversal(); 
       tree.root= new Node(1); 
       tree.root.left= new Node(2); 
       tree.root.right= new Node(3); 
       tree.root.left.left= new Node(4); 
       tree.root.left.right= new Node(5); 
         
       System.out.println("The height of the tree is: "+tree.height(tree.root));
       System.out.println("Level order traversal of binary tree is "); 
       tree.printLevelOrder(); 
    } 

}
