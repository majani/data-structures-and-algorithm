/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Searching;

import java.util.Iterator;
import java.util.LinkedList;

/**
 *
 * @author Admin
 */
public class Reachable {

    private final int V;   // No. of vertices 
    LinkedList<Integer> adj[]; //Adjacency Lists

    public Reachable(int V) {

        this.V = V;
        adj = new LinkedList[V];
        for (int i = 0; i < V; i++) {

            adj[i] = new LinkedList<>();
        }

    }

    void addEdge(int src, int dest) {

        adj[src].add(dest);
    }

    public boolean isReachable(int src, int dest) {

        
        boolean visited[] = new boolean[V];

        LinkedList<Integer> queue = new LinkedList<>();

        visited[src] = true;
        queue.add(src);

        while (!queue.isEmpty()) {

            int u = queue.poll();
            Iterator<Integer> i = adj[u].listIterator();

            while (i.hasNext()) {

                int n = i.next();

                if (n == dest) {

                    return true;
                } else {

                    if (!visited[n]) {
                        visited[n] = true;
                        queue.add(n);
                    }
                }

            }

        }
        return false;
    }
    
    
    public boolean dfsutil(int v, boolean visited[], int dest){
    
        visited[v] = true;
        
        Iterator<Integer> i = adj[v].listIterator();
        
        while(i.hasNext()){
        
            int n = i.next();
            if(n==dest){
            
                return true;
            }else
            
            if(!visited[n]){
            
                dfsutil(n,visited,dest);
            }
        }
        
        return false;
    
    }
    
    public boolean isReachableDFS(int v, int dest){
        
        boolean visited[] = new boolean[V];
    
    
        return dfsutil(v,visited,dest);
    }
    
    

    public static void main(String args[]) {

        Reachable g = new Reachable(4);
        g.addEdge(0, 1);
        g.addEdge(0, 2);
        g.addEdge(1, 2);
        g.addEdge(2, 0);
        g.addEdge(2, 3);
        g.addEdge(3, 3);

        boolean flag = g.isReachableDFS(0, 0);

        int u = 0;
        int v = 1;
        if (g.isReachableDFS(u, v)) {
            System.out.println("There is a path from " + u + " to " + v);
        } else {
            System.out.println("There is no path from " + u + " to " + v);
        };

        u = 1;
        v = 0;
        if (g.isReachableDFS(u, v)) {
            System.out.println("There is a path from " + u + " to " + v);
        } else {
            System.out.println("There is no path from " + u + " to " + v);
        };

    }

}
