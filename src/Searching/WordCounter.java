/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Searching;

/**
 *
 * @author Admin
 */
import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class WordCounter {

    public static void main(String[] args) throws FileNotFoundException, IOException {

        Map<String, Integer> m1 = new HashMap();

        try (BufferedReader br = new BufferedReader(new FileReader("C:\\Users\\Admin\\Documents\\NetBeansProjects\\Sorting\\src\\Searching\\file.txt"))) {
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();

            while (line != null) {
                String[] words = line.split(" ");//those are your words
                for (String word : words) {
                    if (m1.get(word) == null) {
                        m1.put(word, 1);
                    } else {
                     
                        m1.put(word,m1.get(word)+1);
                    }
                }
                sb.append(System.lineSeparator());
                line = br.readLine();
            }
        }
        Map<String, Integer> sorted = new TreeMap<>(m1);
        for (Object key : sorted.keySet()) {
            System.out.println("Word: " + key + "\tCounts: " + m1.get(key));
        }
    }}
