/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Searching;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

/**
 *
 * @author Admin
 */
public class WordFinder {

    public static void main(String[] args) throws IOException {
        String input = "Who";   // Input word to be searched
        int count = 0;   //Intialize the word to zero
        try (BufferedReader br = new BufferedReader(new FileReader("C:\\Users\\Admin\\Documents\\NetBeansProjects\\Sorting\\src\\Searching\\file.txt"))) {

            String s = br.readLine();

            while (s != null) //Reading Content from the file
            {
                String[] words = s.split(" ");  //Split the word using space
                for (String word : words) {
                    if (word.equals(input)) //Search for the given word
                    {
                        count++;    //If Present increase the count by one
                    }
                }
            }
        }
        if (count != 0) //Check for count not equal to zero
        {
            System.out.println("The given word is present for " + count + " Times in the file");
        } else {
            System.out.println("The given word is not present in the file");
        }
        //Intialize the word Array
    }
}
