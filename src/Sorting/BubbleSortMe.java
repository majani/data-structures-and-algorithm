/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Sorting;

/**
 *
 * @author Admin
 */
public class BubbleSortMe {

    void bubblesort(int arr[]) {

        int n = arr.length;

        for (int i = 0; i < n; i++) 
            for (int j = 0; j < n - 1; j++) 
                if (arr[j] > arr[j + 1]) {
                    int temp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = temp;
                }
    }
    
    
    void insertionSort(int arr[]){
        
        int n = arr.length;
        for (int i = 1; i < n; ++i) {
            int key = arr[i];
            int j = i - 1;

           
            while (j >= 0 && arr[j] > key) {
                arr[j + 1] = arr[j];
                j = j - 1;
            }
            arr[j + 1] = key;
        }
    
    }

    void printAll(int[] arr) {
        int n = arr.length;
        for (int i = 0; i < n; i++) {
            System.out.println(arr[i] + " ");

        }

    }

    public static void main(String[] args) {
        BubbleSortMe ob = new BubbleSortMe();
        int arr[] = {65, 43, 23, 67, 89, 5, 12, 90};
        ob.bubblesort(arr);
        ob.printAll(arr);

    }
}
